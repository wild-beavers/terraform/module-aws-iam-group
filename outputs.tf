#####
# Group
#####

output "aws_iam_group" {
  value = var.iam_group_enable ? { for k, v in aws_iam_group.this["0"] :
    k => v if !contains(["tags"], k)
  } : null
}

#####
# Policy
#####

output "aws_iam_policy" {
  value = var.iam_policy_enable ? { for k, v in aws_iam_policy.this["0"] :
    k => v if !contains(["tags", "attachment_count", "name_prefix", "policy"], k)
  } : null
}

#####
# Role
#####

output "aws_iam_role" {
  value = var.iam_role_enable ? { for k, v in aws_iam_role.this["0"] :
    k => v if !contains(["tags", "assume_role_policy", "inline_policy", "managed_policy_arns", "name_prefix", "permissions_boundary"], k)
  } : null
}
