data "aws_partition" "this" {}

data "aws_caller_identity" "this" {}

data "aws_organizations_organization" "this" {}

data "aws_iam_policy_document" "example" {
  statement {
    effect = "Allow"
    actions = [
      "ec2:*",
    ]
    resources = ["*"]
  }
}
