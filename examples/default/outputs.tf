#####
# Minimal
#####

output "minimal" {
  value = module.minimal
}

#####
# Advanced
#####

output "advanced" {
  value = module.advanced
}
