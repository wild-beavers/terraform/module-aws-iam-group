locals {
  prefix = "${random_string.start_letter.result}${random_string.this.result}"
}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

#####
# Minimal
#####

module "minimal" {
  source = "../../"

  prefix = local.prefix
}

#####
# Advanced
# - use account_ids
# - restrict to current organization
# - attach supplementary roles
# - add additional document to the policy
# - attach external policies to created group and role
# - additional tags
#####

module "advanced" {
  source = "../../"

  prefix = local.prefix

  aws_partition = data.aws_partition.this.partition
  account_ids = [
    data.aws_caller_identity.this.account_id,
  ]
  organization_id          = data.aws_organizations_organization.this.id
  restrict_to_organization = true

  iam_group_name = "test"
  iam_group_path = "/a/b/c/"
  iam_group_policy_arns = {
    0 = "arn:aws:iam::aws:policy/ReadOnlyAccess"
  }

  iam_policy_name = "test"
  iam_policy_path = "/path/"
  iam_policy_additional_allowed_switch_roles = [
    "arn:aws:iam:::role/actions/EC2ActionsAccess",
  ]
  iam_policy_additional_document = data.aws_iam_policy_document.example.json
  iam_policy_tags = {
    specific_to_policy = "true"
  }

  iam_role_name = "test"
  iam_role_policy_arns = {
    0 = "arn:aws:iam::aws:policy/ReadOnlyAccess"
  }
  iam_role_tags = {
    specific_to_role = "true"
  }

  tags = {
    cause = "Terraform test"
  }
}
