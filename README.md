# Module AWS IAM Group

Module to create an IAM group along with its role and a policy to allow its members to switch role.

## CAVEAT

- Because this module use `aws_iam_group_policy_attachment` itself, if you wish to add a policy to the group, you need to use the same resource.
  Meaning: `aws_iam_policy_attachment` cannot be used with the group created in this module.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.9 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.15 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 3.15 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group) | resource |
| [aws_iam_group_policy_attachment.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group_policy_attachment) | resource |
| [aws_iam_group_policy_attachment.this_base](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group_policy_attachment) | resource |
| [aws_iam_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_policy_document.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.this_assume_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_partition.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/partition) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_account_ids"></a> [account\_ids](#input\_account\_ids) | List of “trusted” account IDs.<br/>The role created by this module will be assumable from all accounts listed.<br/>The policy created by this module will allow to assume the role in all listed accounts.<br/>If this list is empty, no restriction will be applied ; meaning the IAM group members will be able to assume `var.iam_role_name` in ANY account (except if `var.restrict_to_current_organization` is `true`)." | `list(string)` | `[]` | no |
| <a name="input_aws_partition"></a> [aws\_partition](#input\_aws\_partition) | Current AWS partition. If not given, will be fetched from data source. | `string` | `""` | no |
| <a name="input_iam_group_enable"></a> [iam\_group\_enable](#input\_iam\_group\_enable) | Whether to create the IAM group. Generally, the group needs to be created only once contrary to roles. | `bool` | `true` | no |
| <a name="input_iam_group_name"></a> [iam\_group\_name](#input\_iam\_group\_name) | Name of the group to create. Ignored if `var.iam_group_enable` is `false`. | `string` | `"example"` | no |
| <a name="input_iam_group_path"></a> [iam\_group\_path](#input\_iam\_group\_path) | Path of the IAM group. | `string` | `"/"` | no |
| <a name="input_iam_group_policy_arns"></a> [iam\_group\_policy\_arns](#input\_iam\_group\_policy\_arns) | ARNs of the policies to attach to the IAM Group. Keys are ignored. Ignored if `var.iam_group_enable` is `false`. | `map(string)` | `{}` | no |
| <a name="input_iam_policy_additional_allowed_switch_roles"></a> [iam\_policy\_additional\_allowed\_switch\_roles](#input\_iam\_policy\_additional\_allowed\_switch\_roles) | List of role ARNs to be added in the IAM policy linked to the group. The IAM group members will be allowed to assume these roles. The roles can be in any accounts. | `list(string)` | `[]` | no |
| <a name="input_iam_policy_additional_document"></a> [iam\_policy\_additional\_document](#input\_iam\_policy\_additional\_document) | JSON document that will be merged with the default document of the IAM policy. By default, the policy only allow to switch role. This will be ignored if `var.iam_policy_enable` is `false.` | `string` | `null` | no |
| <a name="input_iam_policy_enable"></a> [iam\_policy\_enable](#input\_iam\_policy\_enable) | Whether ot not to create the IAM policy to link to the group. By default, this policy will allow the `var.iam_group_name` to assume the `var.iam_role_name` in all the `var.account_ids`. Generally, the policy needs to be created only once alongside the group in the main account. This policy document can be extended by using `var.iam_policy_additional_document`. | `bool` | `true` | no |
| <a name="input_iam_policy_name"></a> [iam\_policy\_name](#input\_iam\_policy\_name) | Name of the policy to create for the group. Ignored if `var.iam_policy_enable` is `false`. | `string` | `"example-policy"` | no |
| <a name="input_iam_policy_path"></a> [iam\_policy\_path](#input\_iam\_policy\_path) | Path of the IAM policy. | `string` | `"/"` | no |
| <a name="input_iam_policy_tags"></a> [iam\_policy\_tags](#input\_iam\_policy\_tags) | Tags specific to the IAM policy. Will be merge with `var.tags`. | `map(string)` | `{}` | no |
| <a name="input_iam_role_enable"></a> [iam\_role\_enable](#input\_iam\_role\_enable) | Whether ot not to create the IAM role that will be assumable by the `var.iam_group_name`. Generally, the roles should be created in all the accounts. | `bool` | `true` | no |
| <a name="input_iam_role_name"></a> [iam\_role\_name](#input\_iam\_role\_name) | Name of the role to create, corresponding to the IAM group. Ignored if `var.iam_role_enable` is `false`. | `string` | `"example-role"` | no |
| <a name="input_iam_role_path"></a> [iam\_role\_path](#input\_iam\_role\_path) | Path of the IAM role. | `string` | `"/"` | no |
| <a name="input_iam_role_policy_arns"></a> [iam\_role\_policy\_arns](#input\_iam\_role\_policy\_arns) | ARNs of the policies to attach to the IAM role. Keys are ignored. Ignored if `var.iam_role_enable` is `false`. | `map(string)` | `{}` | no |
| <a name="input_iam_role_tags"></a> [iam\_role\_tags](#input\_iam\_role\_tags) | Tags specific to the IAM role. Will be merge with `var.tags`. | `map(string)` | `{}` | no |
| <a name="input_organization_id"></a> [organization\_id](#input\_organization\_id) | Id of the organization where members of the group must be restricted to. Mandatory if `restrict_to_current_organization` is set. | `string` | `""` | no |
| <a name="input_prefix"></a> [prefix](#input\_prefix) | Prefix to be used for all resources names. Specifically useful for tests. | `string` | `""` | no |
| <a name="input_restrict_to_organization"></a> [restrict\_to\_organization](#input\_restrict\_to\_organization) | Whether to restrict members of the IAM group to assume role only within the current organization. This restriction is additional with `var.account_ids`: it does not replace it. | `bool` | `false` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags to be used in every resources created by this module. | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_aws_iam_group"></a> [aws\_iam\_group](#output\_aws\_iam\_group) | n/a |
| <a name="output_aws_iam_policy"></a> [aws\_iam\_policy](#output\_aws\_iam\_policy) | n/a |
| <a name="output_aws_iam_role"></a> [aws\_iam\_role](#output\_aws\_iam\_role) | n/a |
<!-- END_TF_DOCS -->

## Versioning
This repository follows [Semantic Versioning 2.0.0](https://semver.org/)

## Git Hooks
This repository uses [pre-commit](https://pre-commit.com/) hooks.
