#####
# General
#####

locals {
  aws_partition = var.aws_partition == "" ? data.aws_partition.current["0"].partition : var.aws_partition
}

data "aws_partition" "current" {
  for_each = var.aws_partition == "" ? { 0 = 0 } : {}
}

#####
# Policy
#####

data "aws_iam_policy_document" "this" {
  for_each = var.iam_policy_enable ? { 0 = 0 } : {}

  source_policy_documents = var.iam_policy_additional_document == null ? [] : [var.iam_policy_additional_document]

  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole",
    ]
    resources = sort(flatten([
      coalescelist(
        formatlist(provider::aws::arn_build(local.aws_partition, "iam", "", "%s", "role${var.iam_role_path}${local.iam_role_name}"), [for account_id in var.account_ids : account_id]),
        [provider::aws::arn_build(local.aws_partition, "iam", "", "", "role${var.iam_role_path}${local.iam_role_name}")]
      ),
      try(aws_iam_role.this["0"].arn, []),
      var.iam_policy_additional_allowed_switch_roles,
    ]))

    dynamic "condition" {
      for_each = var.restrict_to_organization ? [1] : []

      content {
        test     = "StringEquals"
        values   = [var.organization_id]
        variable = "aws:PrincipalOrgID"
      }
    }
  }
}

#####
# Role
#####

data "aws_iam_policy_document" "this_assume_role" {
  for_each = var.iam_role_enable ? { 0 = 0 } : {}

  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      identifiers = sort(coalescelist(formatlist(provider::aws::arn_build(local.aws_partition, "iam", "", "%s", "root"), [for account_id in var.account_ids : account_id]), ["*"]))
      type        = "AWS"
    }
  }
}
