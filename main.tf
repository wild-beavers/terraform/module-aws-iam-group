locals {
  tags = merge(
    var.tags,
    {
      managed-by = "terraform"
      origin     = "gitlab.com/wild-beavers/terraform/module-aws-iam-group"
    },
  )
}

#####
# Group
#####

locals {
  iam_group_name = format("%s%s", var.prefix, var.iam_group_name)
}

resource "aws_iam_group" "this" {
  for_each = var.iam_group_enable ? { 0 = 0 } : {}

  name = local.iam_group_name
  path = var.iam_group_path
}

#####
# Policy
#####

locals {
  iam_policy_name = format("%s%s", var.prefix, var.iam_policy_name)
}

resource "aws_iam_policy" "this" {
  for_each = var.iam_policy_enable ? { 0 = 0 } : {}

  policy      = data.aws_iam_policy_document.this["0"].json
  name        = local.iam_policy_name
  description = "${local.iam_group_name} group/role policy."
  path        = var.iam_policy_path

  tags = merge(local.tags, var.iam_policy_tags)
}

resource "aws_iam_group_policy_attachment" "this_base" {
  for_each = var.iam_policy_enable && var.iam_group_enable ? { 0 = 0 } : {}

  group      = aws_iam_group.this["0"].name
  policy_arn = aws_iam_policy.this["0"].arn
}

resource "aws_iam_group_policy_attachment" "this" {
  for_each = var.iam_group_enable ? var.iam_group_policy_arns : {}

  group      = aws_iam_group.this["0"].name
  policy_arn = each.value
}

#####
# Role
#####

locals {
  iam_role_name = format("%s%s", var.prefix, var.iam_role_name)
}

resource "aws_iam_role" "this" {
  for_each = var.iam_role_enable ? { 0 = 0 } : {}

  name               = local.iam_role_name
  path               = var.iam_role_path
  description        = "${local.iam_group_name} group role. All members of the group can assume this role."
  assume_role_policy = data.aws_iam_policy_document.this_assume_role["0"].json

  tags = merge(local.tags, var.iam_role_tags)
}

resource "aws_iam_role_policy_attachment" "this" {
  for_each = var.iam_role_enable ? var.iam_role_policy_arns : {}

  role       = aws_iam_role.this["0"].name
  policy_arn = each.value
}
