## 1.0.2

- maintenance: migrate module to Terraform registry
- chore: bump pre-commit hooks
- fix: rename `examples/disabled` directory to `examples/disable`

## 1.0.1

- fix: output condition on role was incorrect, resulting in error when `iam_role_enable` is `true`

## 1.0.0

- feat: (BREAKING) requires `var.organization_id` when `var.restrict_to_organization` is set
- feat: (BREAKING) renames `var.restrict_to_current_organization` to `var.restrict_to_organization`
- feat: (BREAKING) outputs changes:
   - `iam_group_xxx` => `aws_iam_group.xxx`
   - `iam_policy_xxx` => `aws_iam_policy.xxx`
   - `iam_role_xxx` => `aws_iam_role.xxx`
- feat: (BREAKING) adds a description to policy.
- tech: (BREAKING) requires tf `1.9`+
- feat: greatly improve existing variables validations
- feat: adds `var.iam_policy_tags` and `var.iam_role_tags`
- feat: adds w`var.iam_role_path` and `var.iam_policy_path` and `var.iam_group_path`
- feat: adds non-required `var.aws_partition`

## 0.0.3

- fix: allow AWS variables in `var.iam_policy_additional_allowed_switch_roles`
- chore: bump pre-commit hooks

## 0.0.2

- maintenance: fix TFLINT issues
- refactor: change resources and datas `count` to `for_each`
- fix: replace deprecated `source_json` to `source_policy_documents` for `aws_iam_policy_document` data.
- chore: bump pre-commit hooks
- chore: change pre-commit hooks deprecated `git` scheme to `https`
- test: adds gitlab ci

## 0.0.1

- fix: makes sure `var.tags` is merged with local tags.

## 0.0.0

- Initial version
